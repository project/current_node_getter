<?php

namespace Drupal\current_node_getter;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Class CurrentNodeGetter
 * @package Drupal\current_node_getter
 */
class CurrentNodeGetter
{

  function __construct(CurrentRouteMatch $currentRouteMatch, EntityTypeManager $entityTypeManager, LanguageManagerInterface $languageManager)
  {
    $this->currentRouteMatch = $currentRouteMatch;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
  }

  private CurrentRouteMatch $currentRouteMatch;
  private EntityTypeManager $entityTypeManager;
  private LanguageManagerInterface $languageManager;

  /**
   * get current node entity.
   * @return Node|null
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function getEntity(): ?Node
  {

    $is_preview = $this->currentRouteMatch->getRouteName() === 'entity.node.preview';
    /** @var Node */
    $node = $this->currentRouteMatch->getParameter($is_preview ? 'node_preview' : 'node');

    // get current revision.
    $revision = $this->currentRouteMatch->getRawParameter('node_revision');
    if ($node && !empty($node->id()) && $revision) {
      /** @var Node */
      $node = $this->entityTypeManager
        ->getStorage('node')
        ->loadRevision($revision);
    }

    // @todo this code is not work.
    // $lang = $this->languageManager->getCurrentLanguage();
    // if ($node && $lang && $node->hasTranslation($lang->getId())) {
    //   /** @var \Drupal\node\Entity\Node */
    //   $node = $node->getTranslation($lang->getId());
    // }

    return $node;
  }
}
